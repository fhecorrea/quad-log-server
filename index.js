// TODO: Refactor!
const env = require('dotenv').config();
const { createServer } = require('http');
const uuid = require('uuid-mongodb');

const express = require('express');
const mongodb = require('mongoose');
const { json } = require('body-parser');
const { Server } = require('socket.io');

const app = express();

// Tenta se conectar ao banco de dados
mongodb.connect(process.env.MONGODB_SERVER, { useNewUrlParser: true, useUnifiedTopology: true });
mongodb.connection.on('error', console.error.bind(console, 'conn error: '));
mongodb.connection.on('open', function () {
  console.info('Conexão com o banco de dados estabelecida com sucesso...!');
});

const server = createServer(app);
const io = new Server(server);

const userSchema = new mongodb.Schema({
  _id: { // Ver: https://dzone.com/articles/uuids-with-mongodb-and-nodejs
    type: 'object',
    value: { type: 'Buffer' },
    default: () => uuid.v4()
  },
  name: {
    type: String,
    default: 'Anonymous user'
  },
  emailAddress: String,
  passwd: String,
  active: {
    type: Boolean,
    default: true
  },
  wasConfirmed: {
    type: Boolean,
    default: true
  },
  authorizedConsumers: Array
});

userSchema.path('emailAddress').index({ unique: true });

const UserModel = mongodb.model('User', userSchema);

const getCurrentUser = ({ headers }) => {
  // TODO: Adicionar .all
  return {};
};

app.set('view engine', 'pug');
app.use(json());

app.get('/', (req, res) => {
  res.render('example', {
    message: 'Hello world!'
  });
});

app.get('/home', (req, res, next) => {
  const info = getCurrentUser(req);

  if (!info.id) {
    res.status(401).send({ status: 'Not authorized!' });
    return;
  }

  UserModel.findOne({ _id: uuid.from(info.id) })
    .then(user => {
      res.send(user);
    })
    .catch(err => {
      res.status((res && res.statusCode) || 500).send({ status: (res && res.statusMessage) || 'Failed' });
      console.log('error0:', err);
    });
});

const getAxiosOpts = () => {
  const opts = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  };

  return opts;
};

app.post('/login', (req, res, next) => {
  const { username, password } = req.body;
  //console.log(username, password, (!username || !password || password.length < 4));
  const http400Resp = { status: 'Invalid username or password!' };
  if (!username || !password || password.length < 4) {
    res.status(400).send(http400Resp);
    return;
  }

  UserModel.findOne({ emailAddress: username })
    .then(function (foundUser) {
      if (foundUser && foundUser.passwd === password) {
        res.send({
          id: uuid.from(foundUser._id).toString(),
          name: foundUser.name,
          email_address: foundUser.emailAddress,
          active: foundUser.active,
          was_confirmed: foundUser.wasConfirmed,
          authorized_consumer: true,
        });
      } else {
        res.status(400).send(http400Resp);
      }
    })
    .catch(err => {
      res.status(err.response && err.response.statusCode || 500).send({ status: "Login error!" });
      console.log(err.response && err.response.data || err);
    });
});

app.post('/users/create', (req, res) => {

  if (!consumer) {
    res.status(401).send({ status: "Invalid consumer!" });
    return;
  }

  const { email, password, name } = req.body;
  console.log(email, password, (!email || !password || password.length < 4));
  const http400Resp = { status: "Some required fields wasn't send!" };

  if (!email || !password || password.length < 4) {
    res.status(400).send(http400Resp); return;
  }

  UserModel.exists({ emailAddress: email })
    .then((result) => {
      if (result) {
        res.status(403).send({ status: "E-mail already exists! " });
        return;
      }

      const user = new UserModel({
        name,
        emailAddress: email,
        passwd: password,
        authorizedConsumers: [consumer]
      });

      // Sincroniza o novo usuário com o servidor
      user.save();

      res.status(201).send({
        id: uuid.from(user._id).toString(),
        name: user.name,
        email_address: user.emailAddress,
        active: user.active,
        was_confirmed: user.wasConfirmed,
        authorized_consumer: true,
      });
    })
    .catch(err => res.status(500).send(err));

});

app.post('/notification/:room', (req, res) => {
  console.log('body', req.body);
  setTimeout(() => {
    io.to('all').emit('notification', req.body);
    io.to(req.params.room).emit('notification', req.body);
  }, 3000);
  res.send({
    sent: 1
  });
});


app.post('/sale/:room', (req, res) => {

  io.to('all').emit('sale', req.body);

  io.to(req.params.room).emit('sale', req.body);

  res.send({
    sent: 1,
    body: req.body
  });
});

io.on('connection', (socket) => {
  console.log('connected');
  socket.on('listen', room => {
    console.log('listen to', room);
    socket.emit('hi', room);
    socket.join(room);
  });

  socket.on('chat message', (msg) => {
    console.log('message: ' + msg);
    setTimeout(() => {
      io.to(socket.id).emit('retorno', {
        "message": msg + "!"
      });
    }, 2000);
  });

  socket.on("disconnect", (pp) => {
    console.debug(pp);
    //socket.to(room).emit("bye");
    //socket.leave or removeListener
  });
});

server.listen(process.env.PORT, '0.0.0.0', () => {
  console.log('Server escutando em https://localhost:' + process.env.PORT.toString());
});