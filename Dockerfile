FROM node:12
WORKDIR /app
COPY . /app/
RUN npm install 
EXPOSE 8088
CMD ["node", "index.js"]